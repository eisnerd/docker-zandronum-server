# docker-zandronum-server

Zandronum, the multiplayer-oriented source port for DooM, DooM II and many mods, built on Alpine
Linux for a small and easy to use docker image. Host Freedoom, QC:DE, D4T, etc. on your own servers
or in the cloud with a simple docker run.

## Usage

You can build a container with the iwad, wads and config that you want for easy deployment, or you
can just mount a data volume like this:

    docker run -dit --name doom -v ~/.config/zandronum/:/data -p 10666:10666/udp eisnerd/docker-zandronum-server:3.0 +map MAP01

If you supply `-i` and `-t`, as above, then you can control the server with

    docker attach doom

and detach again with `Ctrl-p` then `Ctrl-q`.

## Variants

There are several tagged images available with extras built in. These images include extras
indicated by the parts of the tag name:

* freedm: freedm iwad and all 32 freedm maps in the rotation
* qcde: the freedm iwad plus QC:DE mod and all QC:DE official maps in the rotation
* aeon: the AeonQCDE map pack added to the rotation
* http: a built in http server for downloads of the server's wad/pk3 files
* nomusic: a smaller image that excludes the soundtrack pack

So, the image `3.0-qcde-1.5.0-aeon-music` includes zandronum-server version 3.0, QC:DE version
1.5.0, the AeonQCDE map pack, the QC:DE soundtrack and all the QC:DE official and AeonQCDE maps in
the map rotation. `:qcde-aeon` is exactly the same image right now because `:3.0` is `:latest`,
`:qcde-1.5.0` is the latest `:qcde` available and music is default.

You can run a QC:DE server with all official maps in the rotation and an http server for downloading
the packs like so:

    docker run -dit --name qcde -p 80:80 -p 10666:10666/udp eisnerd/docker-zandronum-server:qcde

## Environment Variables

You can supply these to docker with `-e variable=value`:

* iwad: one iwad to supply to zandronum-server
* wads: zero or more wad or pack files to load. Values are comma separated. File names with spaces
  do not need to be quoted. Example:

      wads=foo.pk3,file with spaces.pk3,other.wad

* opt: zero or more wad or pack files to load as optional
* cmds: extra +command type arguments to supply
* rcon: rcon password for remote admin
* hostname: sets sv_hostname, the name to advertise to server browsers
* website: sets sv_website, for WAD downloads or information
* port: The listening port (UDP) of Zandronum inside the container. Defaults to `10666`. If you plan
  on running multiple instances of the container on the same host machine, due to the way the port
  is advertised to the master server, you must change the listening port of Zandronum via this
  option instead of using a different port mapping in Docker. Example in Docker Compose:

  ```yml
  services:
    instance1:
      image: eisnerd/docker-zandronum-server:3.0
      network_mode: bridge
      ports:
      - 10666:10666/udp
      environment:
      - port=10666
    instance2:
      image: eisnerd/docker-zandronum-server:3.0
      network_mode: bridge
      ports:
      - 10667:10667/udp
      environment:
      - port=10667
  ```

  Note that this example is minimally complete to demonstrate the port configuration only. When you
  use `bridge` networking mode, as shown in the example, you must make sure the host & container
  port mapping is identical if you want the server to be properly visible to the master server list.

If you do not specify the `iwad` enivronment variable, the startup script will look in `/data` or
anything you have mounted underneath there for the usual iwad names, such as `doom2.wad` and
`freedm.wad` and will choose one of those based on my arbitrary, predefined priority list. Other wad
and pack files in `/data` will be used for `-file` unless you set the `wads` environment variable.
Files placed or mounted into `/optional` will be used for the `-optfile` argument, if there are any,
and again you can specify these instead with the `opt` environment variable.

## Master Server

The image is configured to advertise to the master server and by local broadcast by default. You
can, of course, supply your own config file to override this. To allow players out on the internet
to discover your server in, e.g., doomseeker and connect, a few things are necessary:

* expose the doom port with `-p 10666:10666/udp`
* NAT/port forward rules in your router to take 10666 to your host, if applicable
* public IP address discoverable by zandronum

The last point is slightly complicated in docker. The address is not detected by the master server,
which can see the public IP address of your server, but by your zandronum server, which is held
inside a docker container. The usual method used by zandronum to discover the public address to
advertise to the master server will get the container address, a private address like `172.17.0.2`.

To work around this, these images use `ifconfig.co` within the container to find the public IP
address. If you have concerns about the security of this, you need to make sure that the container
hostname resolves to a public IP address, not a docker one. You can do this with `docker run
--network host` or by feeding in a hosts file entry like this:

    docker run --name doom --hostname doom --add-host doom:$(ip address show dev $(ip route|awk '/default/ {print  $5}') up scope global|awk '/inet/ {split($2,addr,"/"); print addr[1]}') -dit -p 10666:10666/udp eisnerd/docker-zandronum-server

Either way, when the image starts, it will note that the hostname resolves to a public IP address
already and will not call `ipconfig.co`.

## Links

* [Zandronum](https://zandronum.com)
* [Zandronum source](https://bitbucket.org/Torr_Samaho/zandronum)
* [docker-zandronum-server](https://hub.docker.com/r/eisnerd/docker-zandronum-server)
* [docker-zandronum-server source](https://gitlab.com/eisnerd/docker-zandronum-server)
