FROM alpine:3.8 AS build

RUN apk --no-cache add cmake make g++ python3 openssl-dev sdl-dev fts-dev

ARG version

RUN apk --no-cache add git coreutils && \
  git config --global user.name "docker-zandronum-server" && \
  git config --global user.email "docker-zandronum-server@example.com" && \
  git clone --depth 5 -b q-zandronum-freedoom https://gitlab.com/eisnerd/zandronum.git && \
  cd zandronum && \
  git remote add genia https://github.com/IgeNiaI/Q-Zandronum && \
  if [ -z $version ]; then git ls-remote --tags --sort=version:refname genia|sed -n 's@.*/v\?\([0-9.]\+\)$@\1\t&@p'|sort -V|tail -1|grep -o 'v\?[0-9.]\+$'; else echo $version; fi|{ read version && \
  istag=$(git ls-remote --tags genia|grep -q /$version$ && echo -n tag) && \
  git fetch genia $istag $version || git fetch origin $istag $version; } && \
  git rebase -Xtheirs FETCH_HEAD && \
  git reset --soft FETCH_HEAD

RUN cd zandronum && \
  mkdir -p build && \
  cd build && \
  cmake -DCMAKE_BUILD_TYPE=Release -DSERVERONLY=1 -Wno-dev .. && \
  make && \
  mkdir -p /app/bin/ && \
  [ -e zandronum-server ] || ln -s *zandronum-server zandronum-server && \
  cp -P *zandronum-server *.pk3 /app/bin/

RUN cd /app && \
  mkdir -p lib usr/lib && \
  ldd bin/zandronum-server |sed '/ld-musl/d;s/.*=> //;s/ (0x.*//'|while read i; do cp "$i" ".$i"; done

FROM alpine:3.8 as base
RUN apk --no-cache add bash tini
COPY --from=build /app /
ENV iwad= \
    wads= \
    opt= \
    cmds= \
    rcon= \
    port= \
    hostname="Dock in DooM" \
    website="gitlab.com/eisnerd/docker-zandronum"
RUN mkdir -p /data /optional $HOME/.config/ && \
  ln -s /data $HOME/.config/zandronum
ONBUILD COPY ./data /data
ONBUILD COPY ./optional /optional
EXPOSE 10666/udp
COPY ./entrypoint.sh /bin/entrypoint.sh
ENTRYPOINT ["/sbin/tini", "--", "/bin/entrypoint.sh"]

FROM base as freedm
RUN cd /data && \
  wget -q https://api.github.com/repos/freedoom/freedoom/releases/latest -O- | \
    grep -o https://.*freedm-.*zip | sort -u | \
    while read i; do wget -q $i -O- | unzip -; done
COPY freedm.cfg /config
ENV hostname "Free the DooM"

FROM freedm as qcde
RUN cd /data && \
  wget -q -O- https://www.moddb.com/mods/quake-champions-doom-edition/downloads | \
    grep -vi Aeon | \
    grep -vi standalone | \
    grep -v Soundtrack | \
    egrep -v 'Half Lives|Heretic|Doom 3|Marathon' | \
    sed -n 's@.*href="/mods/[^"]*/downloads/.*media.moddb.com.*downloads/[^/]*/[^/]*/@@p' | \
    sed 's@/.*@@' | \
    while read i; do \
      wget -q -O- https://www.moddb.com/downloads/start/$i; \
    done | \
    grep -o '/downloads/mirror[^"]*' | \
    sort -u | \
    while read i; do \
      wget -q -O- https://www.moddb.com/$i | unzip -; \
    done && \
  rm -f QCDEmaps* && \
  wget -O QCDEqzpatch2.7.pk3 'https://allfearthesentinel.net/zandronum/download.php?file=qcdeqzpatch2.7.pk3' && \
  wget -O QCDEmaps2.7.1_b01.pk3 https://allfearthesentinel.net/zandronum/download.php?file=qcdemaps2.7.1_b01.pk3 && \
  wget -O QCDEmaps2.7.1_b01_qcde09_fix.pk3 'https://allfearthesentinel.net/zandronum/download.php?file=qcdemaps2.7.1_b01_qcde09_fix.pk3' && \
  mkdir -p /optional && \
  ls -1 *--* *Announce* *mus[^1]* 2> /dev/null |xargs -rII mv I /optional && \
  rm -f *Example*
COPY qcde.cfg /config
ARG qcde_version=1.5.1
ENV qcde_version="$qcde_version"
ENV hostname "Q-Zandronum QC:DE official maps - QCDE $qcde_version - Quake Champions: Doom Edition"

FROM qcde as qcdemus
RUN cd /data && \
  wget -q -O- https://www.moddb.com/mods/quake-champions-doom-edition/downloads | \
    grep -i Soundtrack | \
    sed -n 's@.*href="/mods/[^"]*/downloads/.*media.moddb.com.*downloads/[^/]*/[^/]*/@@p' | \
    sed 's@/.*@@' | \
    while read i; do \
      wget -q -O- https://www.moddb.com/downloads/start/$i; \
    done | \
    grep -o '/downloads/mirror[^"]*' | \
    sort -u | \
    while read i; do \
      wget -q -O- https://www.moddb.com/$i | unzip -; \
    done && \
  mkdir -p /optional && \
  ls -1 *--* *Announce* *mus[^1]* 2> /dev/null |xargs -rII mv I /optional && \
  rm -f *Example*

FROM qcde as aeonqcde
RUN cd /data && \
  wget -q -O- https://www.moddb.com/mods/quake-champions-doom-edition/downloads | \
    grep -i Aeon | \
    sed -n 's@.*href="/mods/[^"]*/downloads/.*media.moddb.com.*downloads/[^/]*/[^/]*/@@p' | \
    sed 's@/.*@@' | \
    while read i; do \
      wget -q -O- https://www.moddb.com/downloads/start/$i; \
    done | \
    grep -o '/downloads/mirror[^"]*' | \
    sort -u | \
    while read i; do \
      wget -q -O- https://www.moddb.com/$i | unzip -; \
    done && \
  mkdir -p /optional && \
  ls -1 *--* *Announce* *mus[^1]* 2> /dev/null |xargs -rII mv I /optional && \
  rm -f *Example*
COPY aeonqcde.cfg /config
ENV hostname "Q-Zandronum QC:DE Aeon maps - AeonQCDE - QCDE $qcde_version - Quake Champions: Doom Edition"

FROM aeonqcde as aeonqcdemus
COPY --from=qcdemus /*/QCDEmus* /optional
