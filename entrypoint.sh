#!/bin/bash -x

iwads=(doom2.wad bfgdoom2.wad doom2bfg.wad doom.wad doomu.wad bfgdoom.wad doombfg.wad tnt.wad plutonia.wad doom2f.wad heretic.wad hereticsr.wad hexen.wad hexdd.wad strife1.wad strife.wad sve.wad chex.wad doom1.wad heretic1.wad hexdemo.wad hexendemo.wad strife0.wad blasphem.wad blasphemer.wad chex3.wad action2.wad harm1.wad hacx.wad hacx2.wad square1.pk3 delaweare.wad freedoom2.wad freedoom.wad freedoom1.wad freedoomu.wad freedm.wad)

containsElement () { for e in "${@:2}"; do [[ "$e" = "$1" ]] && return 0; done; return 1; }

cd /data
config=
if [ -f /config ]; then
  config=(+exec /config)
fi
if [ -z "${wads[*]}" ]; then
  shopt -s nullglob
  wads=(*{wad,zip,7z,pk{3,7}})
  shopt -u nullglob
  for i in "${!wads[@]}"; do
    containsElement "${wads[$i]}" "${iwads[@]}" &&
    unset "wads[$i]"
  done
  reverseWads() { wads=("${BASH_ARGV[@]}"); }
  shopt -s extdebug
  reverseWads "${wads[@]}"
  shopt -u extdebug
else
  IFS="," read -r -a wads <<< "$wads"
fi
[ "${wads[*]}" ] && wads=(-file "${wads[@]}")

if [ -z "${opt[*]}" ]; then
  shopt -s nullglob
  opt=(/optional/*{wad,zip,7z,pk{3,7}})
  shopt -u nullglob
fi
[ "${opt[*]}" ] && opt=(-optfile "${opt[@]}")

if [[ ! -f "$iwad" ]]; then
  for i in "${iwads[@]}" "${iwads[-1]}"; do
    [ -n "$iwad" ] && iwad="$i"
    [ -f "$iwad" ] && break
    iwad="$(find -depth -iname "$iwad"|sed s@^\./@@\;1q)"
    [ -f "$iwad" ] && break
    iwad="$i"
  done
fi

# check whether hostname resolves to a public ip address
if ip address show dev $(ip route|awk '/default/ {print  $5}') up scope global|awk '/inet/ {split($2,addr,"/"); print addr[1]}'|egrep -q '192.168\.|172\.|10\.'; then
  # attempt to find a public one using an external service
  # passing it to zandronum by editing the hosts file
  cp /etc/hosts{,.orig}
  sed "/$(hostname)$/s/.*/$(wget -q -O- ifconfig.co) $(hostname)/" /etc/hosts.orig > /etc/hosts
fi

[ -n "$port" ] && port=(-port $port)

if [[ "$@" ]] && [ "${1#[+-]}" = "$1" ]; then
  exec "$@"
else
  exec /bin/zandronum-server \
    -iwad "$iwad" "${wads[@]}" "${opt[@]}" \
    +sv_rconpassword "$rcon" \
    +sv_hostname "$(eval echo "$hostname")" \
    +sv_website "$website" \
    "${port[@]}" \
    "${config[@]}" $cmds "$@"
fi
