#!/bin/bash -x

mkdir -p /http
cd /http
eval find /{data,optional} -iname index.\* -o\ -iname\ *{wad,zip,7z,pk{3,7}} | xargs -n1 -II ln -sf I ./
httpd

cd /
/bin/entrypoint.sh
