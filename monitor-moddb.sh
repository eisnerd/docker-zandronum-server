#!/bin/bash -x

touch versions

while

qcde_version=$(wget -q -O- https://www.moddb.com/mods/quake-champions-doom-edition/downloads/qc-doom-edition-v10-full-release|grep -o 'QCDE.*zip'|grep -o '[0-9.]*\>')

v="$(<versions)"
(echo "$v"; wget -q -O- https://hub.docker.com/r/eisnerd/docker-zandronum-server/tags/|grep -o '"tags":[^}]*'|grep -o 'qcde-[0-9.]\+'|grep -o '[0-9.]*')|sort -u > versions

# If moddb has a qcde version that is not mentioned in any of the docker hub tag names
fgrep -q $qcde_version versions || {
	# Rebuild gitlab.com/eisnerd/docker-zandronum-server#master
	wget --method=POST -q -O- 'https://gitlab.com/api/v4/projects/8189904/ref/master/trigger/pipeline?token=77bfdd13e8fc9ef502a54075814ef1'

	# Assume the rebuild will be successful and record the version we expect to be built
	# so as to avoid rebuilding before the push to docker hub has completed
	# Ideally, we would clean this up later in case of failed builds
	# or builds that pick up a version other than the one we detected on moddb
	echo $qcde_version >> versions
}
sleep 10m

do continue; done
